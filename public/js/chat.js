const socket = io();
// Elements
const $messageForm = document.querySelector("#message-form");
const $messageFormInput = document.querySelector("input")
const $messageFormButton = document.querySelector("button")
const $sendLocationButton = document.querySelector("#sendLocation")
const $message = document.querySelector("#message")

// templates
const messageTemplate = document.querySelector("#message_template").innerHTML
const locationMessageTemplate = document.querySelector("#location_message_template").innerHTML
const sidebarTemplate = document.querySelector("#sidebar-template").innerHTML
// Options
const { username, room } = Qs.parse(location.search, { ignoreQueryPrefix: true })

const autoscroll = () => {
    const $newMessage = $message.lastElementChild

    const newMessageStyles = getComputedStyle($newMessage);
    const newMessageMargin = parseInt(newMessageStyles.marginBottom)
    const newMessageHeight = $newMessage.offsetHeight + newMessageMargin

    const visibleHeight = $message.offsetHeight
    const containerHeight = $message.scrollHeight
    const scrollOffset = $message.scrollTop + visibleHeight

    if (containerHeight - newMessageHeight <= scrollOffset) {
        $message.scrollTop = $message.scrollHeight
    }

}



socket.on("message", (message) => {
    console.log(message);
    const html = Mustache.render(messageTemplate,
        {
            username: message.username,
            message: message.text,
            createdAt: moment(message.createdAt).format('h:mm a')
        });
    $message.insertAdjacentHTML('beforeend', html)
    autoscroll()
})
socket.on("locationMessage", (message) => {
    const html = Mustache.render(locationMessageTemplate,
        {
            username: message.username,
            url: message.url,
            createdAt: moment(message.createdAt).format('h:mm a')
        })
    $message.insertAdjacentHTML("beforeend", html)
    autoscroll()

})

socket.on('roomData', ({ room, users }) => {
    const html = Mustache.render(sidebarTemplate, {
        room,
        users
    })
    document.querySelector("#sidebar").innerHTML = html
})

$messageForm.addEventListener("submit", (e) => {
    e.preventDefault();
    $messageFormButton.setAttribute("disabled", "disabled")

    // disable Button
    const message = e.target.elements.message.value;
    socket.emit("sendMessage", message, (err) => {
        $messageFormButton.removeAttribute("disabled")
        $messageFormInput.value = '';
        $messageFormInput.focus()
        if (err) {
            return console.log(err);
        }
    })
})

$sendLocationButton.addEventListener("click", () => {
    if (!navigator.geolocation) {
        return alert("geolocation is not supported by your browser")
    }

    $sendLocationButton.setAttribute("disabled", "disabled")
    navigator.geolocation.getCurrentPosition((position) => {
        socket.emit('sendLocation', {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        }, () => {
            $sendLocationButton.removeAttribute("disabled")
        })

    })
})


socket.emit('join', { username, room }, (error) => {
    if (error) {
        alert(error)
        location.href = '/'
    }
})

